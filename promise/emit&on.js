// 发布订阅
const event = {
  //全部事件里面的数组
  arr: [],
  eventLoop(eventArr) {
    eventArr.map(fn => fn());
  },
  on(name, fn) {
    const ev = this.arr.find(item => item.name == name);
    if (ev) {
      ev.fn.push(fn);
      return;
    }
    this.arr.push({ name, fn: [fn] });
  },
  emit(name) {
    if (name) {
      const ev = this.arr.find(item => item.name == name);
      ev && this.eventLoop(ev.fn);
      return;
    }
    this.arr.map(item => {
      item.fn.map(fn => fn());
    });
  }
};
event.on("aa", () => {
  console.log("输出aa");
});
event.on("bb", () => {
  console.log("bb");
});
event.on("bb", () => {
  console.log("cc");
});

event.emit();
