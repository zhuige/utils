

class Subject {
    constructor(){
        this.state = '很开心'
        this.arr = []
    }
    attach(o) {
        this.arr.push(o)
    }
    setState(newState){
        this.state= newState
        this.arr.map(o=>o.update(newState));
    }
}


// 观察者
class Observer {
    constructor(name){
        this.name= name
    }
    update(newState){
        console.log(this.name+'的小宝宝新状态,',newState)
    }
}
let s = new Subject('小宝宝')
let o1 = new Observer('我');
let o2 = new Observer('我媳妇');
s.attach(o1)
s.attach(o2)
s.setState('不开心')
s.setState('又开心了')