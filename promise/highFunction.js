




// AOP 切片编程

function say(who) {
  console.log(who+"哈哈");
}
Function.prototype.before = function(beforeFunction) {
    return (...args)=>{ //newFn
        beforeFunction()
        this(...args)
    }

};

let newFn = say.before(function() {
  console.log("Before");
});
newFn('个个')